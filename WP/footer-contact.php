<?php wp_footer(); ?>
<div class="row pageFooter">
  <div class="col-xs-12 contact-footer-div">
    <p><?php $page = get_page_by_title( 'Footer' ); echo "$page->post_content"; ?></p>
  </div>
</div>
</div>
<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Flowtype.js/1.1.0/flowtype.min.js"></script>
<script>
$(document).ready(function () {
    $('nav.hidden').fadeIn(100).removeClass('hidden');
});
</script>
<script>
  $('body').flowtype({
    minimum: 500,
    maximum: 1200,
    minFont: 18,
    maxFont: 40,
    fontRatio: 30
  });
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
<script>
  $("#Bars").click(function(event) {
    event.preventDefault();
    $("#mobileMenu").toggleClass("mobileMenuBG");
    $("#logo").toggleClass("hide");
    $("#number").toggleClass("hide");
    $(".nav-mobile").toggleClass("nav-mobile-open");
  });
</script>
</body>

</html>
