<?php
/*
Template Name: Point Page
*/
?>
<?php get_header(); ?>
      <div class="col-sm-12 search-intro">
        <div id="search-intro-text">
          <h1 class="follow text-xs-center"><?php the_block( 'Intro Text', array('type' => 'one-liner','apply_filters' => false,) ); ?></h1>
        </div>
      </div>
      <div class="col-sm-5 page-content">
        <div class="card card-block">
	<h1 class="grab"><?php the_block( 'Lead', array('type' => 'one-liner','apply_filters' => false,) ); ?></h1>
	<?php the_block( 'Page Description'); ?>
        </div>
      </div>
      <div class="col-sm-7 points total-<?php the_block( 'Number of Cards', array('type' => 'one-liner','apply_filters' => false,) ); ?>">
        <div class="card-trio card-block">
          <h1>
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa <?php the_block( 'Card-1 Icon', array('type' => 'one-liner','apply_filters' => false,)  ); ?> fa-stack-1x fa-inverse"></i>
            </span>
          </h1>
          <h2><?php the_block( 'Card-1', array('type' => 'one-liner','apply_filters' => false,)  ); ?></h2>


        </div>
        <div class="card-trio card-block">
          <h1>
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa <?php the_block( 'Card-2 Icon', array('type' => 'one-liner','apply_filters' => false,)  ); ?> fa-stack-1x fa-inverse"></i>
            </span>
          </h1>
          <h2><?php the_block( 'Card-2', array('type' => 'one-liner','apply_filters' => false,)  ); ?></h2>

        </div>
        <div class="card-trio card-block">
          <h1>
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa <?php the_block( 'Card-3 Icon', array('type' => 'one-liner','apply_filters' => false,)  ); ?> fa-stack-1x fa-inverse"></i>
            </span>
          </h1>
          <h2><?php the_block( 'Card-3', array('type' => 'one-liner','apply_filters' => false,)  ); ?></h2>


        </div>
        <div class="card-trio card-block">
          <h1>
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa <?php the_block( 'Card-4 Icon', array('type' => 'one-liner','apply_filters' => false,)  ); ?> fa-stack-1x fa-inverse"></i>
            </span>
          </h1>
          <h2><?php the_block( 'Card-4', array('type' => 'one-liner','apply_filters' => false,)  ); ?></h2>
        </div>
        <div class="card-trio card-block">
          <h1>
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa <?php the_block( 'Card-5 Icon', array('type' => 'one-liner','apply_filters' => false,)  ); ?> fa-stack-1x fa-inverse"></i>
            </span>
          </h1>
          <h2><?php the_block( 'Card-5', array('type' => 'one-liner','apply_filters' => false,)  ); ?></h2>


        </div>
        <div class="card-trio card-block analytics-contact">
          <h1 class="emailUs">Get Started - Contact Us</h1>
          <p>To increase lead volume, cut waste, improve efficiency, and capture new data to help you get ahead of your competition - contact us today.</p>
          <a href="/contact">
            <button type="button" class="btn btn-primary btn-lg emailUsBtn">CONTACT US</button>
          </a>
        </div>
      </div>
  <?php get_footer(); ?>
