<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags always come first -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <?php wp_head(); ?>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body>
  <div id="background" class="bg1"></div>
  <div id="mobileMenu">
    <a href="/">Home</a>
    <br>
    <a href="/search">Search</a>
    <br>
    <a href="/optimization">Optimization</a>
    <br>
    <a href="/analytics">Analytics</a>
    <br>
    <a href="/contact">Contact</a>
    <br>
    <span><a href="tel:+1-415-917-3330"><i class="fa fa-phone"></i> 415-917-3330</a></span>

  </div>
  <div class="container-fluid page">
    <a href="/">
      <img id="logo" src="<?php $upload_dir = wp_upload_dir(); echo $upload_dir['baseurl']; ?>/logo.svg">
    </a>
    <div class="row">
      <div class="col-sm-12 nav">
        <nav class="hidden">
          <a href="/search">Search</a>
          <a href="/optimization">Optimization</a>
          <a href="/analytics">Analytics</a>
          <a href="/contact">Contact</a>
          <span><a href="tel:+1-415-917-3330"><i class="fa fa-phone"></i> 415-917-3330</a></span>
        </nav>
      </div>
      <div class="col-sm-12 nav-mobile">
        <a id="number" href="tel:+1-415-917-3330"><i class="fa fa-phone"></i> 415-917-3330</a>
        <nav>
          <a id="Bars" href=""><i class="fa fa-bars"></i></a>
        </nav>
      </div>
