<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>
      <div class="col-sm-12 home-intro">
        <img id="browser" src="<?php bloginfo('stylesheet_directory'); ?>/net.svg ?>">
        <div id="home-intro-text">
	<h1 class="lead"><?php the_block( 'Lead', array('type' => 'one-liner','apply_filters' => false,) ); ?></h1>
	<?php the_block( 'Follow', array('type' => 'one-liner',)); ?>
	<p class="brief"><?php the_block( 'Brief', array('apply_filters' => false,) ); ?></p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 home-content">
        <div class="col-sm-4">
	  <h1>
          <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-search fa-stack-1x fa-inverse"></i>
            </span>
          </h1>
	  <h2><?php the_block( 'Page 1 Text', array('type' => 'one-liner',) ); ?></h2>
	<a href="<?php the_block( 'Page 1 Link', array('type' => 'one-liner','apply_filters' => false,) ); ?>">
            <button type="button" class="btn btn-primary btn-lg btn-block">READ MORE</button>
          </a>
        </div>
        <div class="col-sm-4">
	<h1>
          <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-rocket fa-stack-1x fa-inverse"></i>
            </span>
          </h1>
	<h2><?php the_block( 'Page 2 Text', array('type' => 'one-liner',) ); ?></h2>
          <a href="<?php the_block( 'Page 2 Link', array('type' => 'one-liner','apply_filters' => false,) ); ?>">
            <button type="button" class="btn btn-primary btn-lg btn-block">READ MORE</button>
          </a>
        </div>
        <div class="col-sm-4">
	<h1>
          <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-line-chart fa-stack-1x fa-inverse"></i>
            </span>
          </h1>
	<h2><?php the_block( 'Page 3 Text', array('type' => 'one-liner',) ); ?></h2>
	  <a href="<?php the_block( 'Page 3 Link', array('type' => 'one-liner','apply_filters' => false,) ); ?>">
            <button type="button" class="btn btn-primary btn-lg btn-block">READ MORE</button>
          </a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 home-contact">
        <div class="contact-card">
          <div class="col-xs-3">
            <div class="envelope-icon-wrapper">
              <span class="fa-stack fa-lg envelope-icon">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
              </span>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="envelope-icon-wrapper">
              <span class="fa-stack fa-lg envelope-icon">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
              </span>
            </div>
          </div>
          <div class="col-xs-9">
            <h1 class="contact-heading"><?php the_block( 'Contact Heading', array('type' => 'one-liner','apply_filters' => false,) ); ?></h1>
            <p class="contact-description"><?php the_block( 'Contact Description', array('apply_filters' => false,) ); ?></p>
            <a href="/contact">
              <button type="button" class="btn btn-primary btn-lg">CONTACT US</button>
            </a>
          </div>
          <div class="col-xs-12">
            <h1 class="contact-heading"><?php the_block( 'Contact Heading', array('type' => 'one-liner','apply_filters' => false,) ); ?></h1>
            <p class="contact-description"><?php the_block( 'Contact Description', array('apply_filters' => false,) ); ?></p>
	   <a href="/contact">
            <button type="button" class="btn btn-primary btn-lg">CONTACT US</button>
	   </a>
          </div>
        </div>
      </div>
    </div>
<?php get_footer('home'); ?>
