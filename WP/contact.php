<?php
/*
Template Name: Contact Page
*/
?>
<?php get_header(); ?>
      <div class="col-sm-12 contact-intro">
        <div id="contact-intro-text">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 page-content contact-page">
        <div class="envelope-icon-wrapper">
          <span class="fa-stack fa-lg envelope-icon">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
          </span>
        </div>
        <div class="col-sm-10 col-sm-push-1 mail-wrapper">
          <div class="contact-description">
		<h1 class="grab"><?php the_block( 'Lead', array('type' => 'one-liner','apply_filters' => false,) ); ?></h1>
            <p class="text-xs-left"><?php the_block( 'Description', array('apply_filters' => false,) ); ?></p>
		<p class="text-xs-left">
              	<span class="grab">CALL US: </span><?php the_block( 'Call Us', array('type' => 'one-liner','apply_filters' => false,) ); ?></p>
            	<p class="text-xs-left">
              	<span class="grab">EMAIL US: </span><?php the_block( 'Email Us', array('type' => 'one-liner','apply_filters' => false,) ); ?></p><br>
            	<p class="text-xs-left">
              	<span class="grab">LOCATION: </span><?php the_block( 'Location', array('type' => 'one-liner','apply_filters' => false,) ); ?></p>
          </div>
          <form id="contact" action="//formspree.io/<?php the_block( 'Contact Form Address', array('type' => 'one-liner','apply_filters' => false,) ); ?>" method="POST">
            <input class="contact-input" type="email" name="email" placeholder="EMAIL*" required>
            <input class="contact-input" type="text" name="phone" placeholder="PHONE">
            <textarea class="contact-input" type="textfield" placeholder="WHAT'S ON YOUR MIND?" required></textarea>
            <input class="contact-input" type="submit" value="Send">
          </form>
          <?php the_block( 'Bottom Message' ); ?>
        </div>
      </div>
    </div>
  <?php get_footer('contact'); ?>
